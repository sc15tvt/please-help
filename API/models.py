from django.db import models
from django.contrib.auth.models import User


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    first_name = models.CharField(max_length=64, default='')
    surname = models.CharField(max_length=64, default='')
    phone = models.CharField(max_length=64, default='')
    type = models.CharField(
        max_length=64,
        choices=[('personal', 'Personal Account'), ('business',
                                                    'Business Account')], default='personal')


class Account(models.Model):
    customer = models.ForeignKey(
        Customer, related_name="customer_ref", on_delete=models.CASCADE)
    number = models.CharField(max_length=64, unique=True)
    balance = models.DecimalField(max_digits=15, decimal_places=2)
    current_account = models.BooleanField()


class Transaction(models.Model):
    date = models.DateField()
    taken_from = models.ForeignKey(
        Account, related_name="taken_from", on_delete=models.PROTECT)
    given_to = models.ForeignKey(
        Account, related_name="given_to", on_delete=models.PROTECT)
    reference = models.BigIntegerField(unique=True)
    amount = models.DecimalField(max_digits=15, decimal_places=2)


class Invoice(models.Model):
    reference = models.BigIntegerField(unique=True)
    reciever = models.ForeignKey(
        Account, related_name="reciever", on_delete=models.PROTECT)
    stamp = models.CharField(max_length=10, unique=True)
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    paid = models.BooleanField()
    customer_account = models.ForeignKey(
        Account, related_name="customer_account_ref", on_delete=models.PROTECT)
    date = models.DateField()