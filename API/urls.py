from django.urls import path
from . import views

urlpatterns = [
    path('register/', views.register, name='register new user'),
    path('login/', views.login_view, name='login a user'),
    path('logout/', views.logout_view, name='logout a user'),
    path('deposit/', views.deposit, name='Add money to account'),
    path('balance/', views.balance, name='get balance of accounts'),
    path('createinvoice/', views.create_invoice, name='creates new invoice'),
    path('payinvoice/', views.pay_invoice, name='pay invoice'),
    path('transfer/', views.transfer, name='move money between accounts'),
    path('newaccount/', views.create_account, name='Create new account'),
]