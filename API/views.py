from django.shortcuts import render
from django.http import HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from . import models
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from decimal import Decimal
from datetime import datetime


@csrf_exempt
def register(request):
    json_data = ''

    # make sure we only read POST requests
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body.decode('utf-8'))
        except Exception as err:
            print(err)
            return HttpResponse('Invalid JSON payload', status=503)
    else:
        return HttpResponse('Method unavailable.', status=503)

    try:
        # check if user already exists
        person = User.objects.filter(username=json_data['username']).first()
        if person is None:
            person = User.objects.create_user(json_data['username'],
                                              json_data['email'],
                                              json_data['password'])
            person.save()
        else:
            return HttpResponse('User already exists.', status=503)

        # create the new customer
        account = models.Customer(
            user=person,
            first_name=json_data['first_name'],
            surname=json_data['surname'],
            phone=json_data['phone'],
            type=json_data['customer_type'])
        account.save()
    except Exception as err:
        print(err)
        return HttpResponse('Failed to register.', status=503)

    return HttpResponse("Welcome", status=201)


@csrf_exempt
def login_view(request):
    json_data = ''

    # make sure we only read POST requests
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body.decode('utf-8'))
        except Exception as err:
            print(err)
            return HttpResponse('Invalid JSON payload', status=503)
    else:
        return HttpResponse('Method unavailable.', status=503)

    print(json_data)
    user = authenticate(
        username=json_data["username"], password=json_data["password"])

    if user is not None:
        login(request, user)
        return HttpResponse("Welcome", status=200)
    else:
        return HttpResponse("Invalid credentials", status=403)


@csrf_exempt
def logout_view(request):
    logout(request)
    return HttpResponse('Logged out', status=200)


@login_required
def create_account(request):
    c = models.Customer.objects.get(user=request.user)
    if c is None:
        return HttpResponse("No such customer.", status=503)

    if models.Account.objects.filter(customer=c).count() > 0:
        account = models.Account(
            customer=c,
            number=User.objects.make_random_password(
                length=10, allowed_chars='123456789'),
            balance=0.0,
            current_account=False)
        account.save()
        return HttpResponse("OK", status=200)

    else:
        account = models.Account(
            customer=c,
            number=User.objects.make_random_password(
                length=10, allowed_chars='123456789'),
            balance=0.0,
            current_account=True)
        account.save()
        return HttpResponse("OK", status=200)


@login_required
def deposit(request):
    json_data = ''

    # make sure we only read POST requests
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body.decode('utf-8'))
        except Exception as err:
            print(err)
            return HttpResponse('Invalid JSON payload', status=503)
    else:
        return HttpResponse('Method unavailable.', status=503)

    c = models.Customer.objects.get(user=request.user)
    if c is None:
        return HttpResponse("No such customer.", status=503)

    account = models.Account.objects.get(
        customer=c, number=json_data["account_num"])
    if account is None:
        return HttpResponse("No such account.", status=503)

    try:
        account.balance = account.balance + Decimal(json_data["amount"])
    except Exception as err:
        print(err)
        return HttpResponse('Failed to update balance.', status=503)

    account.save()
    return HttpResponse("Money added to account", status=201)


@login_required
def transfer(request):
    json_data = ''

    # make sure we only read POST requests
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body.decode('utf-8'))
        except Exception as err:
            print(err)
            return HttpResponse('Invalid JSON payload', status=503)
    else:
        return HttpResponse('Method unavailable.', status=503)

    c = models.Customer.objects.get(user=request.user)
    if c is None:
        return HttpResponse("No such customer.", status=503)

    from_acc = models.Account.objects.get(
        customer=c, number=json_data["from_account_num"])
    if from_acc is None:
        return HttpResponse("No such account.", status=503)

    to_acc = models.Account.objects.get(
        customer=c, number=json_data["to_account_num"])
    if to_acc is None:
        return HttpResponse("No such account.", status=503)

    if from_acc.balance < json_data["amount"]:
        return HttpResponse("You don't enough money.", status=503)

    try:
        from_acc.balance = from_acc.balance - Decimal(json_data["amount"])
        to_acc.balance = to_acc.balance + Decimal(json_data["amount"])
        from_acc.save()
        to_acc.save()
    except Exception as err:
        print(err)
        return HttpResponse('Failed to update balance.', status=503)

    response_data = {}
    response_data["account_num"] = from_acc.number
    response_data["balance"] = from_acc.balance
    return HttpResponse("Money added to account", status=201)


@login_required
def balance(request):
    json_data = ''

    # make sure we only read POST requests
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body.decode('utf-8'))
        except Exception as err:
            print(err)
            return HttpResponse('Invalid JSON payload', status=503)
    else:
        return HttpResponse('Method unavailable.', status=503)

    c = models.Customer.objects.get(user=request.user)
    if c is None:
        return HttpResponse("No such customer.", status=503)

    accounts = models.Account.objects.filter(customer=c).values("balance", "number")
    accounts = list(accounts)
    accounts = list(map(lambda x: {"balance": float(x["balance"]), "account_num": x["number"]}, accounts))

    return HttpResponse(json.dumps(accounts), status=200)


@login_required
def create_invoice(request):
    json_data = ''

    # make sure we only read POST requests
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body.decode('utf-8'))
        except Exception as err:
            print(err)
            return HttpResponse('Invalid JSON payload', status=503)
    else:
        return HttpResponse('Method unavailable.', status=503)

    try:
        bus_account = models.Account.objects.get(number=json_data["account_num"])
        ref_account = models.Account.objects.get(number=json_data["client_ref_num"])
    except Exception as err:
        print(err)
        return HttpResponse('Invalid account numbers', status=503)

    s = User.objects.make_random_password(
        length=10, allowed_chars='123456789qwertyuioplkjhgfdsazxcvbnm')
    ref = User.objects.make_random_password(
        length=10, allowed_chars='123456789')

    try:

        invoice = models.Invoice(
            customer_account=ref_account,
            reciever=bus_account,
            amount=Decimal(json_data["amount"]),
            stamp=s,
            paid=False,
            date=datetime.now(),
            reference=ref)

        invoice.save()
    except Exception as err:
        print(err)
        return HttpResponse('Failed to create invoice', status=503)

    return HttpResponse(json.dumps({"payprovider_ref_num": ref, "stamp_code": s}), status=200)



@login_required
def pay_invoice(request):
    json_data = ''

    # make sure we only read POST requests
    if request.method == 'POST':
        try:
            json_data = json.loads(request.body.decode('utf-8'))
        except Exception as err:
            print(err)
            return HttpResponse('Invalid JSON payload.', status=503)
    else:
        return HttpResponse('Method unavailable.', status=503)

    c = models.Customer.objects.get(user=request.user)
    if c is None:
        return HttpResponse("No such customer.", status=503)

    account = models.Account.objects.get(customer=c, current_account=True)
    if account is None:
        return HttpResponse("Customer has no current account.", status=503)

    if account.balance < Decimal(json_data["amount"]):
        return HttpResponse("Customer doesn't have money to pay the invoice.", status=503)

    try:
        account.balance = account.balance - Decimal(json_data["amount"])
        account.save()

        invoice = models.Invoice.objects.get(reference=json_data["client_ref_num"])
        invoice.paid = True;
    except Exception as err:
        print(err)
        return HttpResponse('Failed to update invoice.', status=503)

    return HttpResponse(
        json.dumps({"stamp_code": invoice.stamp}), status=200)
